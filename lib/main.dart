import 'package:flutter/material.dart';
import './pages/map_ant.dart';
import './pages/course_selection.dart';
import './pages/home.dart';
import 'package:flutter/services.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
      ]);
    return MaterialApp(
      title: 'Suunnistus',
      theme: ThemeData(
       
        primarySwatch: Colors.blue,
      ),
      home: CustomCrsPage(),
      //routes: <String, WidgetBuilder>{
      //  CustomCrsPage.route: (context) => CustomCrsPage(),
      //  CourseSelection.route: (context) => CourseSelection(),
      //}
    );
  }
}

